describe('Verifica se possui as paredes para adicionar os valores e é possível inserior os valores e realizar o cálculo', () => {

  beforeEach(() => {
    cy.visit('http://localhost:3000/')
  })

  it('Contém 4 paredes', () => {
    cy.get('h3').should('have.length', 4).each((parede, index) => {
      cy.wrap(parede).should('have.text', `Parede ${index + 1}`);
    });
  });

  it('Contém 4 campos de altura', () => {
    for (let i = 0; i < 4; i++) {
      cy.get('label').eq(i * 4).should('have.text', 'Altura:');
    }
  });

  it('Contém 4 campos de comprimento', () => {
    for (let i = 0; i < 4; i++) {
      cy.get('label').eq(i * 4 + 1).should('have.text', 'Comprimento:');
    }
  });

  it('Contém 4 campos de portas', () => {
    for (let i = 0; i < 4; i++) {
      cy.get('label').eq(i * 4 + 2).should('have.text', 'Porta(s):');
    }
  });

  it('Contém 4 campos de janelas', () => {
    for (let i = 0; i < 4; i++) {
      cy.get('label').eq(i * 4 + 3).should('have.text', 'Janela(s):');
    }
  });

  it('Contém um botão com o texto "Calcular" ', () => {
    cy.get('button').should('have.text', 'Calcular')
  })

  it('Adiciona valores nas paredes e Clica no botão "Calcular" e verifica se o resultado é exibido', () => {

    cy.get('input').eq(0).type('4')
    cy.get('input').eq(1).type('4')
    cy.get('input').eq(2).type('0')
    cy.get('input').eq(3).type('0')
    cy.get('input').eq(4).type('4')
    cy.get('input').eq(5).type('4')
    cy.get('input').eq(6).type('0')
    cy.get('input').eq(7).type('0')
    cy.get('input').eq(8).type('4')
    cy.get('input').eq(9).type('4')
    cy.get('input').eq(10).type('0')
    cy.get('input').eq(11).type('0')
    cy.get('input').eq(12).type('4')
    cy.get('input').eq(13).type('4')
    cy.get('input').eq(14).type('0')
    cy.get('input').eq(15).type('0')

    cy.get('button').click()

    cy.get('p').eq(1).should('have.text', '3 latas de 3,6L')
    cy.get('p').eq(2).should('have.text', '4 latas de 0,5L')
    cy.get('p').eq(3).should('have.text', '12,8L')
  })

  it('Insere valores maior que 50 metros quadrados e verifica se exibe a mensagem de erro: "Parede 1 deve ter entre 1 e 50 metros quadrados."', () => {

    cy.get('input').eq(0).type('50')
    cy.get('input').eq(1).type('4')
    cy.get('input').eq(2).type('0')
    cy.get('input').eq(3).type('0')
    cy.get('input').eq(4).type('4')
    cy.get('input').eq(5).type('4')
    cy.get('input').eq(6).type('0')
    cy.get('input').eq(7).type('0')
    cy.get('input').eq(8).type('4')
    cy.get('input').eq(9).type('4')
    cy.get('input').eq(10).type('0')
    cy.get('input').eq(11).type('0')
    cy.get('input').eq(12).type('4')
    cy.get('input').eq(13).type('4')
    cy.get('input').eq(14).type('0')
    cy.get('input').eq(15).type('0')

    cy.get('button').click()

    cy.on('window:alert', (str) => {
      expect(str).to.equal('Parede 1 deve ter entre 1 e 50 metros quadrados.');
    });
  
  })

  it('Insere janelas que não cabem em uma parede e verifica se exibe a mensagem de erro: "A área total de portas e janelas da parede 1 não deve exceder 50% da área da parede."', () => {

    cy.get('input').eq(0).type('4')
    cy.get('input').eq(1).type('4')
    cy.get('input').eq(2).type('4')
    cy.get('input').eq(3).type('4')
    cy.get('input').eq(4).type('4')
    cy.get('input').eq(5).type('4')
    cy.get('input').eq(6).type('0')
    cy.get('input').eq(7).type('0')
    cy.get('input').eq(8).type('4')
    cy.get('input').eq(9).type('4')
    cy.get('input').eq(10).type('0')
    cy.get('input').eq(11).type('0')
    cy.get('input').eq(12).type('4')
    cy.get('input').eq(13).type('4')
    cy.get('input').eq(14).type('0')
    cy.get('input').eq(15).type('0')

    cy.get('button').click()

    cy.on('window:alert', (str) => {
      expect(str).to.equal('A área total de portas e janelas da parede 1 não deve exceder 50% da área da parede.');
    });
  
  })

   

  it('Insere uma porta e o valor da altura menor que a porta e exibe a mensagem de erro: "A altura da parede 1 deve ser pelo menos 30cm maior que a altura da porta."', () => {

    cy.get('input').eq(0).type('1')
    cy.get('input').eq(1).type('4')
    cy.get('input').eq(2).type('1')
    cy.get('input').eq(3).type('0')
    cy.get('input').eq(4).type('4')
    cy.get('input').eq(5).type('4')
    cy.get('input').eq(6).type('0')
    cy.get('input').eq(7).type('0')
    cy.get('input').eq(8).type('4')
    cy.get('input').eq(9).type('4')
    cy.get('input').eq(10).type('0')
    cy.get('input').eq(11).type('0')
    cy.get('input').eq(12).type('4')
    cy.get('input').eq(13).type('4')
    cy.get('input').eq(14).type('0')
    cy.get('input').eq(15).type('0')

    cy.get('button').click()

    cy.on('window:alert', (str) => {
      expect(str).to.equal('A altura da parede 1 deve ser pelo menos 30cm maior que a altura da porta.');
    });
  
  })

})
