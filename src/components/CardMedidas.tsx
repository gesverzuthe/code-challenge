import style from "@/components/CardMedidas.module.css";
import { useFormContext } from "react-hook-form";

export default function CardMedidas({ index }: { index: number }) {
  const methods = useFormContext();

  return (
    <div className={style.cardContainer}>
      <div>
        <h3>{`Parede ${index + 1}`}</h3>
      </div>
      <div>
        <div className={style.containerInput}>
          <label htmlFor={`altura${index}`}>Altura:</label>
          <input
            id={`altura${index}`}
            {...methods.register(`altura${index}`)}
          />
        </div>
        <div className={style.containerInput}>
          <label htmlFor={`largura${index}`}>Comprimento:</label>
          <input
            id={`largura${index}`}
            {...methods.register(`largura${index}`)}
          />
        </div>
        <div className={style.containerInput}>
          <label htmlFor={`portas${index}`}>Porta(s):</label>
          <input
            type="number"
            id={`portas${index}`}
            {...methods.register(`portas${index}`)}
          />
        </div>
        <div className={style.containerInput}>
          <label htmlFor={`janelas${index}`}>Janela(s):</label>
          <input
            type="number"
            id={`janelas${index}`}
            {...methods.register(`janelas${index}`)}
          />
        </div>
      </div>
    </div>
  );
}
