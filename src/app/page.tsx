"use client";
import CardMedidas from "@/components/CardMedidas";
import styles from "./page.module.css";
import { FormProvider, useForm } from "react-hook-form";
import { ReactNode, useState } from "react";

interface Inputs {
  altura0: number;
  largura0: number;
  portas0: number;
  janelas0: number;
  altura1: number;
  largura1: number;
  portas1: number;
  janelas1: number;
  altura2: number;
  largura2: number;
  portas2: number;
  janelas2: number;
  altura3: number;
  largura3: number;
  portas3: number;
  janelas3: number;
}

const MEDIDA_JANELA = 2.0 * 1.2;
const MEDIDA_PORTA = 0.8 * 1.9;
const ALTURA_MIN_PORTA_PAREDE = 0.3;
const MIN_PORTAS_E_JANELAS = 0.5;
const MIN_M2_PAREDE = 1;
const MAX_M2_PAREDE = 50;
const COBERTURA_TINTA_POR_LITRO = 5;
const TAMANHOS_DAS_LATAS = [18, 3.6, 2.5, 0.5];

export default function Home() {
  const methods = useForm<Inputs>();
  const [resultado, setResultado] = useState({});
  const [totalLitros, setTotalLitros] = useState(0);

  function calcularArea(data: Inputs): number | void {
    const areas = [];

    for (let i = 0; i < 4; i++) {
      const altura = data[`altura${i}` as keyof Inputs];
      const largura = data[`largura${i}` as keyof Inputs];
      const portas = data[`portas${i}` as keyof Inputs];
      const janelas = data[`janelas${i}` as keyof Inputs];

      const areaParede = altura * largura;
      const areaPortas = portas * MEDIDA_PORTA;
      const areaJanelas = janelas * MEDIDA_JANELA;
      const areaTotalPortasEJanelas = areaPortas + areaJanelas;

      if (areaParede < MIN_M2_PAREDE || areaParede > MAX_M2_PAREDE) {
        alert(`Parede ${i + 1} deve ter entre 1 e 50 metros quadrados.`);
        return;
      }

      if (areaTotalPortasEJanelas > MIN_PORTAS_E_JANELAS * areaParede) {
        alert(
          `A área total de portas e janelas da parede ${
            i + 1
          } não deve exceder 50% da área da parede.`
        );
        return;
      }

      if (portas > 0 && altura < MEDIDA_PORTA + ALTURA_MIN_PORTA_PAREDE) {
        alert(
          `A altura da parede ${
            i + 1
          } deve ser pelo menos 30cm maior que a altura da porta.`
        );
        return;
      }

      const areaPintavel = areaParede - areaTotalPortasEJanelas;
      areas.push(areaPintavel);
    }

    const areaTotalPintavel = areas.reduce((acc, curr) => acc + curr, 0);
    return areaTotalPintavel;
  }

  function calcularLitrosDeTinta(areaTotalPintavel: number): void {
    const tintaNecessaria = areaTotalPintavel / COBERTURA_TINTA_POR_LITRO;

    let tintaRestante = tintaNecessaria;

    const latas: { [key: number]: number } = {};

    for (const tamanho of TAMANHOS_DAS_LATAS) {
      const quantidade = Math.floor(tintaRestante / tamanho);
      if (quantidade > 0) {
        latas[tamanho] = quantidade;
        tintaRestante -= quantidade * tamanho;
      }
    }
    if (tintaRestante > 0) {
      latas[TAMANHOS_DAS_LATAS[TAMANHOS_DAS_LATAS.length - 1]] =
        (latas[TAMANHOS_DAS_LATAS[TAMANHOS_DAS_LATAS.length - 1]] || 0) + 1;
    }

    let somaLitros = 0;
    for (const [tamanho, quantidade] of Object.entries(latas)) {
      somaLitros += parseFloat(tamanho) * quantidade;
    }

    setTotalLitros(somaLitros);

    setResultado(latas);
  }

  const onSubmit = (data: Inputs) => {
    const areaTotalPintavel = calcularArea(data);

    if (areaTotalPintavel) {
      calcularLitrosDeTinta(areaTotalPintavel);
    }
  };

  return (
    <main className={styles.main}>
      <div className={styles.containerParede}>
        <h1>Defina os tamanhos das paredes</h1>
        <FormProvider {...methods}>
          <form
            className={styles.form}
            onSubmit={methods.handleSubmit(onSubmit)}
          >
            <div className={styles.cardParedes}>
              {new Array(4).fill(0).map((_, i) => (
                <CardMedidas index={i} key={i} />
              ))}
            </div>
            <button className={styles.button} type="submit">
              Calcular
            </button>
          </form>
        </FormProvider>
      </div>
      <div className={styles.containerInfor}>
        <div>
          <h2>Resultado do Cálculo</h2>
        </div>
        <div className={styles.containerDetalhes}>
          <div className={styles.containerLatas}>
            <h4>Quantidade de latas necessária</h4>
            <p className={styles.textoResultado}>
              {Object.entries(resultado).map(([key, value]) => (
                <p key={key}>
                  {value as ReactNode} latas de {key.replace(".", ",")}L
                </p>
              ))}
            </p>
          </div>
          <div>
            <h4>Total de Litros</h4>
            <p className={styles.textoResultado}>
              {totalLitros > 0 && (
                <p>{totalLitros.toString().replace(".", ",")}L</p>
              )}
            </p>
          </div>
        </div>
      </div>
    </main>
  );
}
