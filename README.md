# Code Challenge

Uma aplicação web que ajuda o usuário a calcular a quantidade de tinta necessária para pintar uma sala.

## Recursos

- Calcular a quantidade de tinta necessária para pintar o ambiente

## Tecnologias Utilizadas

- [Next](https://nextjs.org/)
- [Cypress](https://www.cypress.io/)

## Pré-requisitos

Certifique-se de ter as seguintes ferramentas instaladas em sua máquina:

- [Node.js](https://nodejs.org/) (versão 14 ou superior)
- [npm](https://www.npmjs.com/) (gerenciador de pacotes do Node.js)

## Instalação

1. **Clone o repositório**

   ```bash
   git clone git@gitlab.com:gesverzuthe/code-challenge.git
2. **Entrar na pasta**
   ```bash
   cd code-challenge
3. **Instale as dependências**
   ```bash
   npm install
4. **Inicie o Projeto**
   ```bash
   npm run dev
5. **Para realizar os testes**
    ```bash
    npm run cypress:open
